/*
 * @Author: your name
 * @Date: 2019-11-14 16:51:22
 * @LastEditTime : 2020-01-06 17:33:44
 * @LastEditors  : sueRimn
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\vue.config.js
 */
const path = require("path");
const CompressionWebpackPlugin = require("compression-webpack-plugin");
const productionGzipExtensions = ["js", "css"];
function resolve(dir) {
  return path.join(__dirname, dir);
}
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const SkeletonWebpackPlugin = require("vue-skeleton-webpack-plugin");
module.exports = {
  css: {
    extract: false,
    loaderOptions: {
      sass: {
        prependData: `@import "@/publicStyle/base.scss";
        @import "@/publicStyle/theme_default.scss";
        `
      }
    }
  },
  publicPath: process.env.NODE_ENV === "production" ? "./" : "/",
  assetsDir: "./static",
  devServer: {
    host: "192.168.1.145",
    // 设置默认端口
    port: 8081,
    // // 设置代理
    proxy: {
      "/api": {
        // 目标 API 地址
        target: "https://vote.51takeit.com",
        // target: "https://js.51takeit.com",
        // 将主机标头的原点更改为目标URL
        changeOrigin: true,
        pathRewrite: {
          "^/api": "https://vote.51takeit.com/api"
          // "^/api": "https://js.51takeit.com/api"
        }
      }
    }
  },
  lintOnSave: "warning",
  productionSourceMap: false,
  configureWebpack: {
    externals: {
      BMap: "BMap"
    },
    plugins: [
      new SkeletonWebpackPlugin({
        webpackConfig: {
          entry: {
            app: path.join(__dirname, "./src/common/entry-skeleton.js")
          }
        },
        minimize: true,
        quiet: true,
        router: {
          mode: "hash",
          routes: [
            { path: "/index/goods", skeletonId: "skeleton1" },
            { path: "/index/goodsClass", skeletonId: "skeleton2" }
          ]
        }
      }),
      new CompressionWebpackPlugin({
        // filename: '[path].gz[query]',
        algorithm: "gzip",
        test: new RegExp("\\.(" + productionGzipExtensions.join("|") + ")$"),
        threshold: 10240, //对超过10k的数据进行压缩
        minRatio: 0.6 // 压缩比例，值为0 ~ 1
      })
    ]
  },
  // parallel: false,
  chainWebpack: config => {
    // 设置常用快捷路径
    config.resolve.alias
      .set("@", resolve("src/"))
      .set("assets", resolve("src/assets"))
      .set("components", resolve("src/components"))
      .set("views", resolve("src/views"));
    config.optimization.minimize(true);
    // 设置图片打包规则
    const imagesRule = config.module.rule("images");
    imagesRule
      .use("url-loader")
      .loader("url-loader")
      .tap(options => Object.assign(options, { limit: 18144 }));
    if (process.env.NODE_ENV === "production") {
      // 使用可视化工具分析打包后的模块体积
      config
        .plugin("analyzer")
        .use(
          new BundleAnalyzerPlugin({
            analyzerHost: "127.0.0.1",
            analyzerPort: 8811
          })
        )
        .end();
    }
  }
};

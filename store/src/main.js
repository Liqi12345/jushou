import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "./axios-mock/axios";
import Util from "./common/utils";
// import mock from "./axios-mock/mock";
// import "chinese-gradient";
// import "chinese-layout";
import MD5 from "js-md5";
Vue.prototype.Util = Util;
Vue.prototype.axios = axios;
Vue.prototype.MD5 = MD5;

// mock.init();
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

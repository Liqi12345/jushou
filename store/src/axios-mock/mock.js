var MockAdapter = require("axios-mock-adapter");
var axios = require("axios");

// Mock any GET request to /users
// arguments for reply are (status, data, headers)

export default {
  init() {
    var mock = new MockAdapter(axios);
    mock.onPost("/users").reply(config => {
      console.log(config);
      return { status: 200, config };
    });
  }
};

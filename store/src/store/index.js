import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
function JsonToParse(jsonString) {
  return JSON.parse(jsonString);
}
export default new Vuex.Store({
  state: {
    StoreUserInfo: JsonToParse(sessionStorage.getItem("StoreUserInfo")),
    StoreClassUpdateData: JsonToParse(
      sessionStorage.getItem("StoreClassUpdateData")
    ),
    StoreGoodsUpdateData: JsonToParse(
      sessionStorage.getItem("StoreGoodsUpdateData")
    )
  },
  mutations: {
    changStoreUserInfo(state, payload) {
      state.StoreUserInfo = payload;
      if (payload) {
        sessionStorage.setItem("StoreUserInfo", JSON.stringify(payload));
      }
    },
    changStoreClassUpdateData(state, payload) {
      state.StoreClassUpdateData = payload;
      if (payload) {
        sessionStorage.setItem("StoreClassUpdateData", JSON.stringify(payload));
      }
    },
    changStoreGoodsUpdateData(state, payload) {
      state.StoreGoodsUpdateData = payload;
      if (payload) {
        sessionStorage.setItem("StoreGoodsUpdateData", JSON.stringify(payload));
      }
    }
  },
  actions: {},
  modules: {}
});

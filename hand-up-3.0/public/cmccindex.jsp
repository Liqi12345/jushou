<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ page import="java.io.InputStream"%>
<%@ page import="java.io.BufferedReader"%>
<%@ page import="java.io.InputStreamReader"%>
<%@ page import="com.pascal.xml.system.io.IOTools"%>
<%@ page import="com.pascal.common.util.Debug"%>
<%
	String jsonStr="";
	String jsonUrl="https://js.51takeit.com/wxmp/api/cmcc_area.json?a="+System.currentTimeMillis();
	Debug.println("index2:cmcc_area.json");
	jsonStr=com.jushou.util.Util.getURLMessage(jsonUrl, "UTF-8");
	Debug.println("index2:cmcc_area.json::"+jsonStr);
	
%>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="apple-mobile-web-app-capable" content="yes">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vant@2.2/lib/index.css">
    <script src="./rem.js"></script>
    <script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
    
	<title>移动活动专区</title>
  </head>
  <style type="text/css">
    * {
      margin: 0;
      padding: 0;
    }
    html,
    body {
      position: relative;
      width: 100%;
      height: 100%;
      background: rgb(243, 243, 243);
    }
    img {
      width: 100%;
      height: 100%;
      display: block;
    }
    .textoverflow--Singleline {
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
    .textoverflow--Multiline {
      overflow: hidden;
      display: -webkit-box;
      　　-webkit-box-orient: vertical;
      　　-webkit-line-clamp: 2;
    }
    input::-webkit-input-placeholder {
      color: #000;
    }
    #list {
      width: 100%;
      height: 100%;
      box-sizing: border-box;
      background: #fff;
    }
    .banner {
      width: 5.4rem;
      height: 2.1rem;
      border-radius: 0.06rem;
      overflow: hidden;
      margin: 0 auto 0.06rem;
    }
    .banner a {
      display: block;
      width: 100%;
      height: 100%;
    }
    /* goods */
    .content {
      display: flex;
      justify-content: space-between;
      width: 100%;
      height: 100%;
      /* height: calc(100% - 2.26rem); */
    }
    /* left */
    .left_GoodsClass {
      width: 1.72rem;
      box-sizing: border-box;
      background: #f6f6f6;
    }
    .left_GoodsClass .GoodsClass-item {
      width: 100%;
      height: 0.96rem;
      display: flex;
      align-items: center;
    }
    .left_GoodsClass span {
      margin: auto;
      text-align: center;
      display: flex;
      justify-content: center;
      align-items: center;
      color: #808080;
      font-size: 0.25rem;
      font-family: Source Han Sans CN;
      font-weight: 400;
      cursor: pointer;
    }
    .left_GoodsClass .GoodsClass-item.active {
      background: #ffffff;
    }
    .left_GoodsClass .GoodsClass-item.active span {
      color: #2b2b2b;
      font-size: 0.29rem;
      font-family: Source Han Sans CN;
      font-weight: bold;
      width: 100%;
      border-left: 0.08rem solid #ffd200;
      box-sizing: border-box;
    }
    /* right */
    .right_GoodsInfo {
      width: 5.78rem;
      background: #fff;
      box-sizing: border-box;
    }
    .GOODS_LIST {
      height: calc(100% - 3.26rem);
      overflow-y: scroll;
      touch-action: pan-y;
      -webkit-overflow-scrolling: touch;
    }
    .GOODS_LIST .goodsbox {
      display: flex;
      width: 5.24rem;
      height: 1.85rem;
      box-sizing: border-box;
      align-items: center;
      justify-content: flex-start;
      text-decoration: none;
      outline: none;
      margin: auto;
      position: relative;
    }
    .GOODS_LIST .goodsbox:last-child {
      border-bottom: none;
    }
    .GOODS_LIST .goodsImg {
      width: 1.68rem;
      height: 1.68rem;
      position: relative;
      overflow: hidden;
    }
    .goodsImg .mask {
      background-image: url("./static/goods/goodsmask.png");
      background-position: 0 0;
      background-repeat: no-repeat;
      background-size: 1.15rem 0.77rem;
      width: 1.15rem;
      height: 0.77rem;
      position: absolute;
      left: 0;
      bottom: 0;
      display: flex;
      justify-content: flex-end;
      flex-direction: column;
      align-items: flex-start;
    }
    .maskcon {
      width: 0.85rem;
      height: 0.58rem;
      display: flex;
      justify-content: center;
      flex-direction: column;
      align-items: center;
    }
    .goodsImg .mask span {
      color: #846622;
      font-size: 0.22rem;
      font-weight: 600;
    }

    .goodsInfo {
      width: calc(100% - 1.84rem);
      height: 100%;
      padding-top: 0.1rem;
      margin-left: 0.16rem;
      box-sizing: border-box;
      border-bottom: 0.01rem solid #ebebeb;
    }
    .goodstitle {
      font-size: 0.26rem;
      font-family: SourceHanSansCN-Regular;
      font-weight: 400;
      color: #000000;
      line-height: 0.36rem;
    }
    .goods-desc {
      color: #808080;
      font-size: 0.22rem;
      font-family: Source Han Sans CN;
      font-weight: 400;
    }
    .goodsprice {
      width: 2.78rem;
      height: 0.72rem;
      font-size: 0.22rem;
      font-family: Source Han Sans CN;
      letter-spacing: 0;
      color: #e90000;
      display: flex;
      flex-wrap: wrap;
      justify-content: flex-start;
      align-content: center;
      align-items: baseline;
      margin-top: 0.24rem;
    }
    .goodsprice p {
      font-size: 0.3rem;
      margin-right: 0.1rem;
    }
    .goodsprice .vip-price {
      font-size: 0.24rem;
      color: #2d2d2d;
    }
    .goodsprice p span {
      width: 100%;
      height: 100%;
      font-size: 0.22rem;
    }
    /* TABBAR */
    .tabbar {
      width: 100%;
      position: fixed;
      bottom: 0;
      left: 0;
      z-index: 999;
      display: flex;
      justify-content: space-around;
      align-items: center;
      border-top: 0.01rem solid #c4c4c4;
      height: 1.16rem;
      background: #fff;
    }
    .tabbar li {
      width: 25%;
      height: 100%;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .tabbar li img {
      width: 0.44rem;
      height: 0.44rem;
      margin-bottom: 0.1rem;
    }
    .tabbar li span {
      color: #333;
      line-height: 1;
      font-size: 12px;
    }
    .van-search {
      padding: 0.17rem 0.63rem 0.27rem;
    }
    .van-search__content {
      background-color: #f8f8f8;
      padding-left: 0.24rem;
    }
    .van-field__control {
      font-size: 0.24rem;
    }
  </style>

  <body>
    <div id="list">
      <!-- <div>
        <van-search
          v-model="SearchValue"
          @search="onSearch"
          placeholder="请输入搜索内容"
          shape="round"
        />
      </div> -->
      <div class="content">
        <div class="left_GoodsClass">
          <div
            class="GoodsClass-item"
            v-for="(v,i) in GOODSCLASS"
            :class="active == i?'active':''"
            :key="v"
            @click="setActive(i)"
          >
            <span>{{v}}</span>
          </div>
        </div>
        <div class="right_GoodsInfo">
          <div class="banner">
            <van-swipe
              :autoplay="3000"
              indicator-color="white"
              style="width: 100%;height: 100%;overflow: hidden;"
            >
              <van-swipe-item v-for="(item,index) in BANNER_lIST" :key="index">
                <img
                  :src="item.imgurl"
                  alt=""
                  style="width:100%;height:100%"
                  @click="skip1(item.url)"
                />
              </van-swipe-item>
            </van-swipe>
          </div>

          <div class="GOODS_LIST">
            <div
              class="goodsbox"
              v-for="(v,i) in GOODSlIST[active]"
              :key="i"
              @click="goUrl(v)"
            >
              <div class="goodsImg">
                <img :src="v.title_url" alt="" />
              </div>
              <div class="goodsInfo">
                <div class="goodstitle textoverflow--Singleline">
                  {{v.act_name}}
                </div>
                <div
                  class="goods-desc textoverflow--Singleline"
                  style="height: 0.36rem;"
                >
                  {{v.descr}}
                </div>
                <div class="goodsprice">
                  <p>
                    <span>¥</span
                    ><span style="font-size: 0.3rem;"
                      >{{(v.prize*0.01).toFixed(2)}}</span
                    >
                  </p>

                  <del
                    class="vip-price"
                    v-if="(v.goods_cost*0.01 - v.prize*0.01)>0"
                    style="font-size: 0.24rem;font-weight: 400;"
                  >
                    ¥{{(v.goods_cost*0.01).toFixed(2)}}
                  </del>
                  <img
                    src="./static/goods/goodsitem-shop-icon.png"
                    alt=""
                    style="width: 0.54rem;height: 0.54rem;position: absolute;bottom: 0.23rem;right:0.08rem;"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="tabbar">
        <li @click="skip(v.route)" v-for="(v,i) in tabbar">
          <img :src="v.imgUrl" alt="" /><span>{{v.name}}</span>
        </li>
      </ul>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vant@2.2/lib/vant.min.js"></script>
    <script>
      new Vue({
        el: "#list",
        data: {
          SearchValue: "",
          sub_url: "",
          GOODSCLASS: [],
          GOODSlIST: [],
          BANNER_lIST: [
            {
              url:
                "https://js.51takeit.com/wxmp/index.html#/redirect?state=vip_buy",
              imgurl:
                "http://test-10023814.cos.myqcloud.com/quzou/2020-03-25/26652474.jpg",
            },
            {
              url:
                "https://js.51takeit.com/wxmp/index.html#/redirect?state=backurl&backurl=L3d4bXAvYWN0LzIwMTkxMS9lZ2dGcmVuenkvaW5kZXguaHRtbA",
              imgurl:
                "http://test-10023814.cos.myqcloud.com/quzou/2019-11-24/35678106.jpg",
            },
            {
              url:
                "https://js.51takeit.com/wxmp/index.html#/redirect?state=backurl&backurl=L3d4bXAvYWN0LzIwMTkxMS9MdWNreVdoZWVsL2luZGV4Lmh0bWw",
              imgurl:
                "http://test-10023814.cos.myqcloud.com/quzou/2019-11-24/35727211.jpg",
            },
          ],
          active: sessionStorage.getItem("all_active")
            ? sessionStorage.getItem("all_active")
            : 0,
          tabbar: [
            {
              name: "首页",
              imgUrl: "./static/tabbar/home_nor.png",
              route: "index",
            },
            {
              name: "分类",
              imgUrl: "./static/tabbar/tab_classify_pre.png",
              route: "goodslist",
            },
            {
              name: "购物车",
              imgUrl: "./static/tabbar/shopping_nor.png",
              route: "cart",
            },
            {
              name: "我的",
              imgUrl: "./static/tabbar/my_nor.png",
              route: "user",
            },
          ],
          dialog: false,
        },
        created() {
          var response='<%=jsonStr%>';
        console.log(response);
        	response = JSON.parse(response);
            
            this.GOODSCLASS = response.dataList.map(v=>{ return v.class_name})
            this.GOODSlIST = response.dataList.map(v=>{ return v.dataList})
            this.sub_url = response.sub_url;
        },
        methods: {
          /* 查询 */
          onSearch() {
            sessionStorage.setItem("search", this.SearchValue);
            location.href = "./index.html#/search";
          },
          setActive(i) {
            this.active = i;
            sessionStorage.setItem("all_active", i);
          },
          /* TAB bar 跳转 */
          skip(url) {
            location.href = "https://js.51takeit.com/wxmp/index.html#/" + url;
            // if (this.isWeiXin()) {
              
            // } else {
            //   location.href = this.sub_url;
            // }
          },
          /* banner 跳转 */
          skip1(url) {
            location.href = url;
            // if (this.isWeiXin()) {
            //   //微信环境
            //   location.href = url;
            // } else {
            //   //非微信环境  跳转二维码页面
            //   location.href = this.sub_url;
            // }
          },
          isWeiXin() {
            var ua = window.navigator.userAgent.toLowerCase();
            if (ua.match(/MicroMessenger/i) == "micromessenger") {
              return true;
            } else {
              return false;
            }
          },
          /* 商品跳转 */
          goUrl(e) {
            location.href = e.goods_url;
            // if (this.isWeiXin()) {
            //   //微信环境
              
            // } else {
            //   //非微信环境
            //   location.href = this.sub_url;
            // }
          },
        },
      });
    </script>
    <div style="display: none;">
      <script type="text/javascript">
        var cnzz_protocol =
          "https:" == document.location.protocol ? "https://" : "http://";
        document.write(
          unescape(
            "%3Cspan id='cnzz_stat_icon_1278189573'%3E%3C/span%3E%3Cscript src='" +
              cnzz_protocol +
              "v1.cnzz.com/stat.php%3Fid%3D1278189573%26show%3Dpic1' type='text/javascript'%3E%3C/script%3E"
          )
        );
      </script>
    </div>
  </body>
</html>

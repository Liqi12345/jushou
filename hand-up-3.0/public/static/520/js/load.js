window.game = new Phaser.Game(750, 1220, Phaser.CANVAS, "game");
var Load = function () { };
Load.prototype = {
  startLoad: function () {
    this.load.audio('music_bg', './static/520/music/bg.mp3');
    this.load.audio('gold_down', './static/520/music/gold_down.mp3');
    this.load.audio('gold_turn', './static/520/music/gold_turn.mp3');
    this.load.audio('mouse_bite', './static/520/music/mouse_bite.mp3');
    this.load.audio('mouse_climb', './static/520/music/mouse_climb.mp3');
    this.load.image('dialog', './static/active314/prize_success_bg.png');
    this.load.image('bg', './static/520/bg.png');
    this.load.image('wave', './static/520/wave.png');
    this.load.image('gold_lucky', './static/520/images/gold.png');
    this.load.spritesheet('hoop', './static/520/hoop.png', 600, 1001);
    this.load.spritesheet('clouds', './static/520/clouds.png', 500, 888);
    this.load.spritesheet('fire', './static/520/fire.png', 431, 430);
    this.load.spritesheet('light', './static/520/light.png', 750, 1333);
    this.load.spritesheet('mouse', './static/520/mouse.png', 463, 720);
    this.load.spritesheet('gold', './static/520/gold.png', 229, 199);
    this.load.spritesheet('treasure', './static/520/treasure.png', 310, 200);
    this.load.spritesheet('gold_bg', './static/520/gold_bg.png', 750, 1333);
    this.load.spritesheet('gold01', './static/520/gold1.png', 337, 337);
    this.load.json('cloudArr', './static/520/js/getCloud.json');

    for (let i = 1; i <= 44; i++) {
      this.load.atlas(`gold${i}`, `./static/520/images/${i}.png`, `./static/520/images/${i}.json`);
    }
    this.load.start();
  },
  loadStart: function () {
    this.text.setText("加载中 ...");
  },
  fileComplete: function (progress) {
    this.text.setText(+ progress + "%");
  },
  loadComplete: function () {
    $('#game').hide();
    if (!localStorage.getItem("is_zhiyin")) {
      window.Vue.is_know = 11;
      $(".play_box").css({ height: "100vh", overflow: "hidden" });
    }
    // game.state.start('Game');
  },
  create: function () {
    this.stage.backgroundColor = '#3ba8ae';
    //适应屏幕
    this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
    this.scale.setGameSize(gameWidth * 2, gameHeight * 2);
    this.scale.setMinMax(320, 480, 1080, 1920);
    //失去焦点是否继续游戏
    this.stage.disableVisibilityChange = true;
    //load提示
    this.text = this.add.text(this.world.centerX, this.world.centerY, '', { font: +"300px", fill: '#fff' });
    this.text.anchor.set(0.5);
    this.text.scale.setTo(1.5)
    //this.text.setShadow(3, 3, 'rgba(0,0,0,0.2)', 2);
    this.load.onLoadStart.add(this.loadStart, this);//开始
    this.load.onFileComplete.add(this.fileComplete, this);//加载中
    this.load.onLoadComplete.add(this.loadComplete, this);//加载结束
    this.startLoad();
  }
};
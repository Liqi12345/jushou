let __isDebug__ = true;
var log = {
    /** 输出红色日志 */
    e() {
        if (__isDebug__ && arguments.length > 0) {
            let arr = Array.prototype.slice.call(arguments);
            this.__log("Error", 'red', arr);
        }
    },
    /** 输出黄色日志 */
    w() {
        if (__isDebug__ && arguments.length > 0) {
            let arr = Array.prototype.slice.call(arguments);
            this.__log("Warn", '#FFA500', arr);
        }
    },
    /** 输出绿色日志 */
    i() {
        if (__isDebug__ && arguments.length > 0) {
            let arr = Array.prototype.slice.call(arguments);
            this.__log("Info", '#4b0', arr);
        }
    },
    /** 输出黑色正常日志 */
    d() {
        if (__isDebug__ && arguments.length > 0) {
            let arr = Array.prototype.slice.call(arguments);
            this.__log("Default", '#333', arr);
        }
    },
    /**
     * @param tagStr 没有标签时默认显示的
     * @param color 日志颜色
     * @param arr 日志参数
     */
    __log(tagStr, color, arr) {
        let tag = arr[0];
        if (typeof (tag) == "string" || tag == null || tag == undefined) {
            arr[0] = `%c ${arr[0]}`;
            arr.splice(1, 0, `background:${color};color:#fff`);
        } else {
            arr.splice(0, 0, `%c ${tagStr} : `);
            arr.splice(1, 0, `background:${color};color:#fff`);
        }
        console.log.apply(console, arr);
    }
}
export default log
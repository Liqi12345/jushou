//星座
// import Star from '../views/constellation/star.vue'
// import ConstellationDetail from '../views/constellation/constellationdetail.vue'
import Find from '../views/find/index';//首页
/* 测试题公版 */
/* 
  测试题类型 计分 得分型
*/
import TestTemlpleteScore from '../views/find/testScoreTemplete/index';//首页
import TestTemlpleteScoreSub from '../views/find/testScoreTemplete/subject';//答题页
import TestTemlpleteScoreRes from '../views/find/testScoreTemplete/result';//结果页
/* 
  测试题类型 计分 得分型 不显示分数
*/
import TestTemlplete from '../views/find/testTemplete/index';//首页
import TestTemlpleteSub from '../views/find/testTemplete/subject';//答题页
import TestTemlpleteRes from '../views/find/testTemplete/result';//结果页
/* 
  测试题类型 一道题 
*/
import SkipTestTemplete from '../views/find/skipTestTemplete/index';//首页
import SkipTestTempleteSub from '../views/find/skipTestTemplete/subject';//答题页
import SkipTestTempleteRes from '../views/find/skipTestTemplete/result';//结果页
/* 
  测试题类型 跳转
*/
import SkipTemplete from '../views/find/skipTemplete/index';//首页
import SkipTempleteSub from '../views/find/skipTemplete/subject';//答题页
import SkipTempleteRes from '../views/find/skipTemplete/result';//结果页


import lottery_start from '../views/find/lottery/lottery_start';
import lottery_result from '../views/find/lottery/lottery_result';
import lottery_code from '../views/find/lottery/lottery_code';


const routes = [
    {
        path: '/find',
        name: 'find',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/index.vue")
    },
    /*  测试题类型 计分 得分型 */
    {
        path: '/TestTemlpleteScore',
        name: 'testTemlpleteScore',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/testScoreTemplete/index.vue")
    },
    {
        path: '/TestTemlpleteScoreSub',
        name: 'TestTemlpleteScoreSub',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/testScoreTemplete/subject.vue")
    },
    {
        path: '/TestTemlpleteScoreRes',
        name: 'TestTemlpleteScoreRes',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/testScoreTemplete/result.vue")
    },
    /* 红包 */
    {
        path: '/lottery_start',
        name: 'lottery_start',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/lottery/lottery_start.vue")
    },
    {
        path: '/lottery_result',
        name: 'lottery_result',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/lottery/lottery_result.vue")
    },
    {
        path: '/lottery_code',
        name: 'lottery_code',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/lottery/lottery_code.vue")
    },
    /* 测试题类型 计分 得分型 不显示分数 */
    {
        path: '/TestTemlplete',
        name: 'testTemlplete',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/testTemplete/index.vue")
    },
    {
        path: '/TestTemlpleteSub',
        name: 'TestTemlpleteSub',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/testTemplete/subject.vue")
    },
    {
        path: '/TestTemlpleteRes',
        name: 'TestTemlpleteRes',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/testTemplete/result.vue")
    },
    /* 测试题类型 一道题  */
    {
        path: '/SkipTestTemplete',
        name: 'SkipTestTemplete',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/skipTestTemplete/index.vue")
    },
    {
        path: '/SkipTestTempleteteSub',
        name: 'SkipTestTempleteSub',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/skipTestTemplete/subject.vue")
    },
    {
        path: '/SkipTestTempleteRes',
        name: 'SkipTestTempleteRes',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/skipTestTemplete/result.vue")
    },

    {
        path: '/SkipTemplete',
        name: 'SkipTemplete',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/skipTemplete/index.vue")
    },
    {
        path: '/SkipTempleteSub',
        name: 'SkipTempleteSub',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/skipTemplete/subject.vue")
    },
    {
        path: '/SkipTempleteRes',
        name: 'SkipTempleteRes',
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/find/skipTemplete/result.vue")
    },
];
export default routes

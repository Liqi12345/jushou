// import GetOpenid from '../views/login/get_openid.vue'
// import Bound from '../views/login/bound.vue'
import GetOpenid from '../views/login/UserAuth.vue'
import Bound from '../views/login/UserAuthBound.vue'
import Login from '../views/login/Login.vue'
const routes = [{
  path: '/redirect',
  name: 'get_openid',
  component: GetOpenid
},
{
  path: "/bound",
  name: "bound",
  component: Bound
}, {
  path: "/login",
  name: "login",
  component: Login
}
]
export default routes
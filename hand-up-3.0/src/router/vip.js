const routes = [
  {
    path: "/vip_buy",
    name: "vip_buy",
    component: () =>
      import(/* webpackChunkName: "vip" */ "@/views/users/vip/vip_buy.vue"),
    meta: { requireAuth: true }
  },
  {
    path: "/vip_buy_success",
    name: "vip_buy_success",
    component: () =>
      import(/* webpackChunkName: "vip" */ "@/views/users/vip/vip_buy_success.vue"),
    meta: { requireAuth: true }
  },
  {
    path: "/vip_rule",
    name: "vip_rule",
    component: () =>
      import(/* webpackChunkName: "vip" */ "@/views/users/vip/vip_rule.vue"),
  }
];
export default routes;

/*
 * @Author: your name
 * @Date: 2019-11-09 16:04:31
 * @LastEditTime : 2020-01-07 15:53:46
 * @LastEditors  : sueRimn
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\src\router\index.js
 */

const routes = [
  /* ↓↓↓↓↓ 砍车活动 ↓↓↓↓↓ */
  {
    path: '/kvindex',
    name: 'kvindex',
    component:() =>
    import(/* webpackChunkName: "kvindex" */ "@/views/2020520/start/index.vue"),
    // meta: { requireAuth: true }
  },
  {
    path: '/pre_elec-coupon',
    name: 'pre_elec-coupon',
    component: () =>
      import(/* webpackChunkName: "pre_elec-coupon" */ "@/views/2020520/play/pre_elec-coupon.vue"),
    // meta: { requireAuth: true }
  },
  {
    path: '/elec-coupon',
    name: 'elec-coupon',
    component: () =>
      import(/* webpackChunkName: "elec-coupon" */ "@/views/2020520/play/elec-coupon.vue"),
    // meta: { requireAuth: true }
  },
  {
    path: '/elec-coupon-comprehensive',
    name: 'elec-coupon-comprehensive',
    component: () =>
      import(/* webpackChunkName: "elec-coupon-comprehensive" */ "@/views/2020520/play/elec-coupon-comprehensive.vue"),
    // meta: { requireAuth: true }
  },
  /* 竞猜 */
  {
    path: '/guess',
    name: 'guess',
    component: () =>
      import(/* webpackChunkName: "HelpShare" */ "@/views/2020520/Guess/index.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/GuessList',
    name: 'GuessList',
    component: () =>
      import(/* webpackChunkName: "GuessList" */ "@/views/2020520/Guess/GuessList.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/guess_rule',
    name: 'guess_rule',
    component: () =>
      import(/* webpackChunkName: "guess_rule" */ "@/views/2020520/Guess/guess_rule.vue"),
    meta: { requireAuth: true }
  },
  /* 扫码 */
  {
    path: '/use_code',
    name: 'use_code',
    component: () =>
      import(/* webpackChunkName: "use_code" */ "@/views/2020520/start/use_code.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/scan_code',
    name: 'scan_code',
    component: () =>
      import(/* webpackChunkName: "scan_code" */ "@/views/2020520/start/scan_code.vue"),
    // meta: { requireAuth: true }
  },
  {
    path: '/video',
    name: 'video',
    component: () =>
      import(/* webpackChunkName: "video" */ "@/views/2020520/play/video.vue"),
    meta: { requireAuth: true }
  },
  /* 砍车 */
  {
    path: '/play_index',
    name: 'play_index',
    component: () =>
      import(/* webpackChunkName: "play_index" */ "@/views/2020520/play/play_index.vue"),
    // meta: { requireAuth: true }
  },
  {
    path: '/get_shop',
    name: 'get_shop',
    component: () =>
      import(/* webpackChunkName: "get_shop" */ "@/views/2020520/play/get_shop.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/act_prize',
    name: 'act_prize',
    component: () =>
      import(/* webpackChunkName: "act_prize" */ "@/views/2020520/my/my_prize.vue"),
    // meta: { requireAuth: true }
  },
  /* 奖品清单 */
  {
    path: '/prizelist',
    name: 'prizelist',
    component: () =>
      import(/* webpackChunkName: "prizelist" */ "@/views/2020520/start/prizelist.vue"),
    meta: { requireAuth: true }
  },
  /* 门店地址 分会场地址 */
  {
    path: '/adrlist',
    name: 'adrlist',
    component: () =>
      import(/* webpackChunkName: "adrlist" */ "@/views/2020520/start/adrlist.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/exchange',
    name: 'exchange',
    component: () =>
      import(/* webpackChunkName: "exchange" */ "@/views/2020520/play/exchange.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/sup_exchange',
    name: 'sup_exchange',
    component: () =>
      import(/* webpackChunkName: "sup_exchange" */ "@/views/2020520/play/sup_exchange.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/get_play_num',
    name: 'get_play_num',
    component: () =>
      import(/* webpackChunkName: "get_play_num" */ "@/views/2020520/play/get_play_num.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/push_suc',
    name: 'push_suc',
    component: () =>
      import(/* webpackChunkName: "push_suc" */ "@/views/2020520/my/push_suc.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/push_fail',
    name: 'push_fail',
    component: () =>
      import(/* webpackChunkName: "push_fail" */ "@/views/2020520/my/push_fail.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/prize_suc',
    name: 'prize_suc',
    component: () =>
      import(/* webpackChunkName: "prize_suc" */ "@/views/2020520/my/prize_suc.vue"),
    meta: { requireAuth: true }
  },
  /* 优惠券 */
  {
    path: '/liuyuan',
    name: 'liuyuan',
    component: () =>
      import(/* webpackChunkName: "liuyuan" */ "@/views/2020520/my/liuyuan.vue"),
    // meta: { requireAuth: true }
  },
  {
    path: '/screen',
    name: 'screen',
    component: () =>
      import(/* webpackChunkName: "screen" */ "@/views/2020520/play/screen.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/outer_chain',
    name: 'outer_chain',
    component: () =>
      import(/* webpackChunkName: "get_discount" */ "@/views/2020520/play/outer_chain.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/jushou',
    name: 'jushou',
    component: () =>
      import(/* webpackChunkName: "jushou" */ "@/views/2020520/play/jushou.vue"),
    meta: { requireAuth: true }
  },
  /* 活动核销 */
  {
    path: '/cancelreg',
    name: 'cancelreg',
    component: () =>
      import(/* webpackChunkName: "cancelreg" */ "@/views/2020520/cancel1/reg.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/cancel',
    name: 'cancel',
    component: () =>
      import(/* webpackChunkName: "cancel" */ "@/views/2020520/cancel1/cancel.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/cancelresult',
    name: 'CancelResult',
    component: () =>
      import(/* webpackChunkName: "cancelresult" */ "@/views/2020520/cancel1/cancelresult.vue"),
    meta: { requireAuth: true }
  },
  /* 活动核销 */
  /* 港丽核销 */
  {
    path: '/cancelreg1',
    name: 'cancelreg1',
    component: () =>
      import(/* webpackChunkName: "cancelreg1" */ "@/views/2020520/cancel1/reg.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/cancel1',
    name: 'cancel1',
    component: () =>
      import(/* webpackChunkName: "cancel1" */ "@/views/2020520/cancel1/cancel.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/cancelresult1',
    name: 'CancelResult1',
    component: () =>
      import(/* webpackChunkName: "CancelResult1" */ "@/views/2020520/cancel1/cancelresult.vue"),
    meta: { requireAuth: true }
  },
  /* 港丽核销 */
  {
    path: '/active_rule',
    name: 'active_rule',
    component: () =>
      import(/* webpackChunkName: "active_rule" */ "@/views/2020520/start/active_rule.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/channel',
    name: 'channel',
    component: () =>
      import(/* webpackChunkName: "channel" */ "@/views/2020520/play/channel.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/cut_cancel',
    name: 'cut_cancel',
    component: () =>
      import(/* webpackChunkName: "cut_cancel" */ "@/views/2020520/cancel/cut_cancel.vue"),
    meta: { requireAuth: true }
  },
  // {
  //   path: '/520video',
  //   name: '520video',
  //   component: () =>
  //     import(/* webpackChunkName: "get_discount" */ "@/views/2020520/my/video.vue"),
  //   // meta: { requireAuth: true }
  // },
];

export default routes;

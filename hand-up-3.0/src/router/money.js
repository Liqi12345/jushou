
const routes = [
    {
        path:"/WeChat",
        name:"WeChat",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/money/WeChat.vue")
    },
    {
        path:"/balance",
        name:"balance",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/money/balance.vue")
    },
    {
        path:"/recharge",
        name:"recharge",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/money/recharge.vue")
    },
    {
        path:"/balanceList",
        name:"balanceList",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/money/balanceList.vue")
    },
    {
        path:"/myDistribution",
        name:"myDistribution",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/myDistribution.vue")
    },
    {
        path:"/Partner",
        name:"Partner",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/Partner.vue")
    },


]
export default routes
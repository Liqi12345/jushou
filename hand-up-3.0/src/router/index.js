/*
 * @Author: your name
 * @Date: 2019-11-09 16:04:31
 * @LastEditTime: 2019-12-04 17:34:54
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\src\router\index.js
 */
import Index from '../views/index.vue'
const routes = [
  {
    path: "/index",
    name: "index",
    component:Index,
  },
  {
    path: "/backurl",
    name: "backurl",
    component: () =>
      import(/* webpackChunkName: "backUrl" */ "@/views/login/backurl.vue")
  }
];

export default routes;

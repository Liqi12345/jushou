/*
 * @Author: your name
 * @Date: 2019-11-09 16:04:31
 * @LastEditTime: 2019-11-18 19:24:59
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\src\main.js
 */
import Vue from "vue";
import App from "./App.vue";
import router from "./router.js";
import store from "./store";
import Axios from './axios'

// import VideoPlayer from 'vue-video-player'
// import contrib from 'videojs-contrib-hls'

window.axios = Axios;
window.api = 'https://js.51takeit.com/api' //正式105
// options 为可选参数，无则不传
// Vue.use(VideoPlayer);
// Vue.use(contrib);
Vue.config.productionTip = false;
// 定义一个名为loading的指令
Vue.directive('loading', {
  /**
   * 只调用一次，在指令第一次绑定到元素时调用，可以在这里做一些初始化的设置
   * @param {*} el 指令要绑定的元素
   * @param {*} binding 指令传入的信息，包括 {name:'指令名称', value: '指令绑定的值',arg: '指令参数 v-bind:text 对应 text'}
   */
  bind(el, binding) {
    
    console.log('Bind')
  },
  /**
   * 所在组件的 VNode 更新时调用
   * @param {*} el
   * @param {*} binding
   */
  update(el, binding) {
    
    // 通过对比值的变化判断loading是否显示
    if (binding.oldValue !== binding.value) {
      // el.instance.visible = binding.value
      console.log('update')
    }
  },
  /**
   * 只调用一次，在 指令与元素解绑时调用
   * @param {*} el
   */
  unbind(el) {
    console.log('unbind')
  }
})

window.addEventListener('resize', function () {
  if (
    document.activeElement.tagName === 'INPUT' ||
    document.activeElement.tagName === 'TEXTAREA'
  ) {
    window.setTimeout(function () {
      if ('scrollIntoView' in document.activeElement) {
        document.activeElement.scrollIntoView(false)
      } else {
        document.activeElement.scrollIntoViewIfNeeded(false)
      }
    }, 0)
  }
})
/* 解决标题栏回退时不刷新的问题 */
document.setTitle = function (t) {
  document.title = t;
  var i = document.createElement('iframe');
  i.src = '';
  i.style.display = 'none';
  i.onload = function () {
    setTimeout(function () {
      i.remove();
    }, 9)
  };
  document.body.appendChild(i);
};

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

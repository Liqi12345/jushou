// const { resolve } = require("core-js/fn/promise");

/*
 * @Author: your name
 * @Date: 2019-11-14 16:51:22
 * @LastEditTime : 2020-01-06 17:33:44
 * @LastEditors  : sueRimn
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\vue.config.js
 */
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "./" : "/",
  assetsDir: "./static",
  devServer: {
    host: "192.168.1.145",
    // 设置默认端口
    port: 8081,
    // // 设置代理
    proxy: {
      "/api": {
        // 目标 API 地址
        target: "https://vote.51takeit.com",
        // target: 'https://js.51takeit.com',
        // 将主机标头的原点更改为目标URL
        changeOrigin: true,
        pathRewrite: {
          "^/api": "https://vote.51takeit.com/api"
          // '^/api': 'https://js.51takeit.com/api'
        }
      }
    }
  },
  lintOnSave: false,
  productionSourceMap: false,
  configureWebpack: {
    externals: {
      BMap: "BMap"
    }
  },
  // parallel: false,
  chainWebpack: config => {
    // 设置常用快捷路径
    // config.resolve.alias
    //   .set('@', resolve('src/'))
    //   .set('style', resolve('src/style'));
    config.optimization.minimize(true);
    // 设置图片打包规则
    const imagesRule = config.module.rule("images");
    imagesRule
      .use("url-loader")
      .loader("url-loader")
      .tap(options => Object.assign(options, { limit: 18144 }));
    // 使用可视化工具分析打包后的模块体积
    config
      .plugin('analyzer')
      .use(new BundleAnalyzerPlugin({
        analyzerHost: "127.0.0.1",
        analyzerPort: 8822
      }))
      .end();
  }
};

/*
 * @Author: your name
 * @Date: 2019-11-09 16:04:31
 * @LastEditTime: 2019-11-13 17:52:02
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\babel.config.js
 */
module.exports = {
  presets: ["@vue/cli-plugin-babel/preset"],
  plugins: [
    ['import', {
      libraryName: 'vant',
      libraryDirectory: 'es',
      style: true
    }, 'vant']
  ]
};
